package task_1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class my_class {

    public static void main(String[] args) throws FileNotFoundException {
        Integer[] numbers = new Integer[20];
        int counter = 0;
        Scanner scanner = new Scanner(new File("res//file"));
        String line = scanner.nextLine();
        String[] numbersString = line.split(", ");

        for (String number : numbersString) {
            numbers[counter++] = Integer.parseInt(number);
        }

        System.out.print("Исходный Файл: \n");
        System.out.println(Arrays.toString(numbers));
        Arrays.sort(numbers, Collections.reverseOrder());
        System.out.print("\nОтсортирован в порядке убывания: \n");
        System.out.println(Arrays.toString(numbers));
        scanner.close();

    }
}
