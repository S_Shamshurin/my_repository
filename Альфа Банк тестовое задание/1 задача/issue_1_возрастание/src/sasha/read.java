package sasha;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class read {
    public static void main(String[] args) throws FileNotFoundException {
        int[] numbers = new int[20];
        int counter = 0;
        Scanner scanner = new Scanner(new File("res//file"));
        String line = scanner.nextLine();
        String[] numbersString = line.split(", ");

        for(String number : numbersString) {
            numbers[counter++] = Integer.parseInt(number);
        }

        System.out.print("Исходный Файл: \n");
        System.out.println(Arrays.toString(numbers));
        Arrays.sort(numbers);
        System.out.print("\nОтсортирован в порядке возрастания: \n");
        System.out.println(Arrays.toString(numbers));
        scanner.close();

    }

}
