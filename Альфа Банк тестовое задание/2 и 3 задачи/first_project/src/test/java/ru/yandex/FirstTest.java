package ru.yandex;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public class FirstTest {
    //Объявление переменной
    public ChromeDriver driver;

    @Before
    public void SetUp() {
        //Указание пути к chromedriver
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Slava\\chromedriver.exe");
        //Инициализация переменной
        driver = new ChromeDriver();
    }

    //Задание 2 телефоны
    @Test
    public void Phones() throws InterruptedException {
        //Открытие страницы
        driver.get("https://yandex.ru/");
        //Поиск и клик элемента Маркет
        driver.findElement(By.cssSelector("[href=\"https://market.yandex.ru/?clid=505&utm_source=face_abovesearch&utm_campaign=face_abovesearch\"]")).click();
        //Поиск и клик элемента Электроника
        driver.findElement(By.cssSelector("[href=\"/catalog--elektronika/54440\"]")).click();
        //Поиск и клик элемента Мобильные телефоны
        driver.findElement(By.cssSelector("[href=\"/catalog--mobilnye-telefony/54726/list?catId=91491&hid=91491\"]")).click();
        //Скролл страницы
        ((JavascriptExecutor)driver).executeScript("scroll(0,800)");
        //Активация чекбокса
        driver.findElement(By.cssSelector("[href=\"/catalog/54726/list?hid=91491&glfilter=7893318%3A153061\"]")).click();
        //Ввод диапазона цен
        driver.findElement(By.id("glpricefrom")).sendKeys("40000");
        //Ожидание прогрузки страницы с установленными фильтрами
        Thread.sleep(10000);
        //Запоминаем наименование первого телефона
        String textFirstPagePhone = driver.findElement(By.className("n-snippet-cell2__image")).getAttribute("title");
        //Кликаем по первому телефону
        driver.findElement(By.className("n-snippet-cell2__image")).click();
        //Запоминаем наименование телефона на открытой страничке
        String textSecondPagePhone = driver.findElement(By.xpath("//h1")).getText();
        //Сравнение наименований
        Assert.assertTrue(textSecondPagePhone.equals(textFirstPagePhone));
    }

    //Задание 2 наушники
    @Test
    public void HeadPhones() throws InterruptedException {
        //Открытие страницы
        driver.get("https://yandex.ru/");
        //Поиск и клик элемента Маркет
        driver.findElement(By.cssSelector("[href=\"https://market.yandex.ru/?clid=505&utm_source=face_abovesearch&utm_campaign=face_abovesearch\"]")).click();
        //Поиск и клик элемента Электроника
        driver.findElement(By.cssSelector("[href=\"/catalog--elektronika/54440\"]")).click();
        //Поиск и клик элемента наушники
        driver.findElement(By.cssSelector("[href=\"/catalog--naushniki-i-bluetooth-garnitury/56179/list?catId=90555&hid=90555\"]")).click();
        //Скролл страницы
        ((JavascriptExecutor)driver).executeScript("scroll(0,800)");
        //Активация чекбокса
        driver.findElement(By.cssSelector("[href=\"/catalog/56179/list?hid=90555&glfilter=7893318%3A8455647\"]")).click();
        //Ввод диапазона цен
        driver.findElement(By.id("glpricefrom")).sendKeys("17000");
        driver.findElement(By.id("glpriceto")).sendKeys("25000");
        //Ожидание прогрузки страницы с установленными фильтрами
        Thread.sleep(10000);
        //Запоминаем наименование первых наушников
        String textFirstPageHead = driver.findElement(By.className("n-snippet-cell2__image")).getAttribute("title");
        //Кликаем по первым наушникам
        driver.findElement(By.className("n-snippet-cell2__image")).click();
        //Запоминаем наименование наушников на открытой страничке
        String textSecondPageHead = driver.findElement(By.xpath("//h1")).getText();
        //Сравнение наименований
        Assert.assertTrue(textSecondPageHead.equals(textFirstPageHead));
    }

    //Задание 3 Альфа банк
    @Test
    public void Alpha() throws IOException {
        // Инициализация объекта date
        Date date = new Date();
        //Инициализация объекта formatter, который можно использовать для форматирования даты
        Format formatter = new SimpleDateFormat("YYYY-MM-dd_hh-mm-ss");
        //Открытие гугла
        driver.get("https://www.google.ru/");
        //Поиск поля для ввода
        WebElement element = driver.findElement(By.name("q"));
        //Ввод и отправка данных
        element.sendKeys("альфа банк");
        element.submit();
        // Получить Handles перед открытием новой вкладки
        Set<String> oldHandles = driver.getWindowHandles();
        //Клик по элементу, открывающего новую вкладку
        driver.findElement(By.cssSelector("[href=\"https://alfabank.ru/\"]")).click();
        // Получить Handles после открытия новой вкладки
        Set<String> newHandles = driver.getWindowHandles();
        //Фокус на новую открытую вкладку
        for (String handle : newHandles) {
            if (!oldHandles.contains(handle)) {
                driver.switchTo().window(handle);
            }
        }
        //Скролл страницы
        ((JavascriptExecutor)driver).executeScript("scroll(0,2500)");
        //Thread.sleep(5000);
        //Клик по найденному элементу
        driver.findElement(By.cssSelector("[href=\"http://job.alfabank.ru/\"]")).click();
        //Еще раз скролл
        ((JavascriptExecutor)driver).executeScript("scroll(0,700)");
        //Thread.sleep(5000);
        //Запоминаем нужный текст
        String line1 = driver.findElement(By.xpath("//div/div/p[1]")).getText();
        String line2 = driver.findElement(By.xpath("//div/div/p[2]")).getText();
        String line3 = driver.findElement(By.xpath("//div/div/p[3]")).getText();
        //Создаем файл и записываем в него сохраненный текс
        File file = new File(formatter.format(date)+"_Chrome_" + "Google");
        if(!file.exists())
            file.createNewFile();
        PrintWriter pw = new PrintWriter(file);
        pw.println(line1);
        pw.println(line2);
        pw.println(line3);
        pw.close();
    }

    @After
    public void close() {
        driver.quit();
    }
}
